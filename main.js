function uncheckOtherCheckboxes(checkbox) {
    const checkboxes = document.querySelectorAll('.email--img');
    
    checkboxes.forEach(item => {
      if (item !== checkbox) {
        item.checked = false;
      }
    });
  }
  